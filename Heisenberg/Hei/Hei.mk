##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Hei
ConfigurationName      :=Debug
WorkspacePath          :=D:/Development/Heisenberg
ProjectPath            :=D:/Development/Heisenberg/Hei
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Alex
Date                   :=26/10/2016
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/mingw-w64/x86_64-5.3.0-posix-seh-rt_v4-rev0/mingw64/bin/g++.exe
SharedObjectLinkerName :=C:/mingw-w64/x86_64-5.3.0-posix-seh-rt_v4-rev0/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Hei.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/mingw-w64/x86_64-5.3.0-posix-seh-rt_v4-rev0/mingw64/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/mingw-w64/x86_64-5.3.0-posix-seh-rt_v4-rev0/mingw64/bin/ar.exe rcu
CXX      := C:/mingw-w64/x86_64-5.3.0-posix-seh-rt_v4-rev0/mingw64/bin/g++.exe
CC       := C:/mingw-w64/x86_64-5.3.0-posix-seh-rt_v4-rev0/mingw64/bin/gcc.exe
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := C:/mingw-w64/x86_64-5.3.0-posix-seh-rt_v4-rev0/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/test.cpp$(ObjectSuffix) $(IntermediateDirectory)/Plate.cpp$(ObjectSuffix) $(IntermediateDirectory)/Sandwich.cpp$(ObjectSuffix) $(IntermediateDirectory)/Spin.cpp$(ObjectSuffix) $(IntermediateDirectory)/Heisenberg.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "./Debug"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/Development/Heisenberg/Hei/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix)main.cpp

$(IntermediateDirectory)/test.cpp$(ObjectSuffix): test.cpp $(IntermediateDirectory)/test.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/Development/Heisenberg/Hei/test.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/test.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/test.cpp$(DependSuffix): test.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/test.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/test.cpp$(DependSuffix) -MM test.cpp

$(IntermediateDirectory)/test.cpp$(PreprocessSuffix): test.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/test.cpp$(PreprocessSuffix)test.cpp

$(IntermediateDirectory)/Plate.cpp$(ObjectSuffix): Plate.cpp $(IntermediateDirectory)/Plate.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/Development/Heisenberg/Hei/Plate.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Plate.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Plate.cpp$(DependSuffix): Plate.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Plate.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Plate.cpp$(DependSuffix) -MM Plate.cpp

$(IntermediateDirectory)/Plate.cpp$(PreprocessSuffix): Plate.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Plate.cpp$(PreprocessSuffix)Plate.cpp

$(IntermediateDirectory)/Sandwich.cpp$(ObjectSuffix): Sandwich.cpp $(IntermediateDirectory)/Sandwich.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/Development/Heisenberg/Hei/Sandwich.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Sandwich.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Sandwich.cpp$(DependSuffix): Sandwich.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Sandwich.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Sandwich.cpp$(DependSuffix) -MM Sandwich.cpp

$(IntermediateDirectory)/Sandwich.cpp$(PreprocessSuffix): Sandwich.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Sandwich.cpp$(PreprocessSuffix)Sandwich.cpp

$(IntermediateDirectory)/Spin.cpp$(ObjectSuffix): Spin.cpp $(IntermediateDirectory)/Spin.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/Development/Heisenberg/Hei/Spin.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Spin.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Spin.cpp$(DependSuffix): Spin.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Spin.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Spin.cpp$(DependSuffix) -MM Spin.cpp

$(IntermediateDirectory)/Spin.cpp$(PreprocessSuffix): Spin.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Spin.cpp$(PreprocessSuffix)Spin.cpp

$(IntermediateDirectory)/Heisenberg.cpp$(ObjectSuffix): Heisenberg.cpp $(IntermediateDirectory)/Heisenberg.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/Development/Heisenberg/Hei/Heisenberg.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Heisenberg.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Heisenberg.cpp$(DependSuffix): Heisenberg.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Heisenberg.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Heisenberg.cpp$(DependSuffix) -MM Heisenberg.cpp

$(IntermediateDirectory)/Heisenberg.cpp$(PreprocessSuffix): Heisenberg.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Heisenberg.cpp$(PreprocessSuffix)Heisenberg.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


