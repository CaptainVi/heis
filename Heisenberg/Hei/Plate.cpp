#define _USE_MATH_DEFINES
#include "Plate.h"
#include <string>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <fstream>

//x - width, y - height, z - depth
Plate::Plate(PlateType type, int height, int width, int depth)
{
	Plate::type = type;
	Plate::height = height;
	Plate::width = width;
	Plate::depth = depth;
	Plate::space = std::vector<std::vector<std::vector<Spin> > >(width, std::vector<std::vector<Spin> >(height, std::vector<Spin>(depth, {rnd_theta(),rnd_phi()} )));
}

Plate::~Plate()
{
}

double Plate::rnd(){
	return (double)(rand() / (double)RAND_MAX);
}

double Plate::rnd_theta(){
	return 1.0 / cos(1.0 - 2.0*Plate::rnd());
}

double Plate::rnd_phi(){
	return 2. * M_PI * (Plate::rnd());
}

int Plate::getHeight(){
	return this->height;
}

int Plate::getWidth(){
	return this->width;
}

int Plate::getDepth(){
	return this->depth;
}


Projection Plate::calcMagnetization(){
	int countSpinsInPlate = this->getWidth() * this->getHeight() * this->getDepth();
	double sumSpinX = 0.;
	double sumSpinY = 0.;
	double sumSpinZ = 0.;
	for(int i = 0; i < this->getHeight(); i++)
		for(int j = 0; j < this->getWidth(); j++)
			for(int k = 0; k < this->getDepth(); k++){
				sumSpinX += sin(this->space[i][j][k].theta) * cos(this->space[i][j][k].phi);
				sumSpinY += sin(this->space[i][j][k].theta) * sin(this->space[i][j][k].phi);
				sumSpinZ += cos(this->space[i][j][k].theta);
			}
	Projection projection;
	
	projection.Sx = (1. / (double) countSpinsInPlate) * sumSpinX;
	projection.Sy = (1. / (double) countSpinsInPlate) * sumSpinY;
	projection.Sz = (1. / (double) countSpinsInPlate) * sumSpinZ;

	return projection;
}