#ifndef PLATE_H
#define PLATE_H
#include <string>
#include <vector>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <fstream>

enum PlateType {
	Magn, 
	NoneMagn
};
struct Spin {
	double theta; 
	double phi;
};
struct Projection {
	double Sx;
	double Sy;
	double Sz;
};

class Plate
{
private:
	PlateType type;
	int height;
	int width;
	int depth;
	std::vector< std::vector< std::vector<Spin> > > space;
	double rnd();
public:
	Plate(PlateType type,  int height, int width, int depth);
	~Plate();
	double rnd_theta();
	double rnd_phi();
	int getHeight();
	int getWidth();
	int getDepth();
	Projection calcMagnetization();
};

#endif // PLATE_H
