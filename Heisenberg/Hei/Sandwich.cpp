#include "Sandwich.h"

Sandwich::Sandwich()
{
}

Sandwich::~Sandwich()
{
}

void Sandwich::add(Plate *plate){
	Sandwich::sandwich.push_back(*plate);
}
int Sandwich::countPlates(){
	return Sandwich::sandwich.size();
}

Plate Sandwich::getPlate(int numberPlate){
	if(numberPlate > this->countPlates()-1){
		std::cout << "ERROR: Number of plate more than count of plates";
		exit(1);
	}
	return Sandwich::sandwich[numberPlate];
}

