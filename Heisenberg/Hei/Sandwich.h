#ifndef SANDWICH_H
#define SANDWICH_H
#include <vector>
#include "Plate.h"
class Sandwich
{
private:
	std::vector<Plate> sandwich;
public:
	void add(Plate *plate);
	Plate getPlate(int numberPlate);
	int countPlates();
	
	Sandwich();
	~Sandwich();
};

#endif // SANDWICH_H
