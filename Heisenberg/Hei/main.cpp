#include <cstdio>
#include <iostream>
#include <cstdlib>
#include "Plate.h"
#include "Sandwich.h"
#include <vector>
#include <ctime>
#include "Heisenberg.h"

using namespace std;

int main()
{
		srand(time(NULL));
		Sandwich heis;
		Plate plate1(Magn, 5, 5, 3);
		Plate plate2(Magn, 5, 5, 3);
		Plate plate3(Magn, 5, 5, 3);
		Heisenberg heisenberg;
		heisenberg.hello();
		heis.add(&plate1);
		heis.add(&plate2);
		heis.add(&plate3);
		int count = heis.countPlates();
		cout << "hey and hello from main.cpp count = "<<count;
		for (int i = 0;i < 3;i++) 
		{
			Projection magn = heis.getPlate(i).calcMagnetization();
			cout << "\nMagnetization " <<  i << " plate:\n\tfor x = " << magn.Sx << "\n\tfor y = " << magn.Sy << "\n\tfor z = " << magn.Sz;
		}

		cin.get();
		return 0;
}