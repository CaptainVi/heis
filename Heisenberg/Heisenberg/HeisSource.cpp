#include <iostream>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <fstream>

using namespace std;

class Heisenberg {
#define LENGTH 10
#define L 10
#define N 3
#define COUNT 2
#define PI 3.14159265358979323846
private:
	double space[COUNT][L][L][N][2];
	struct Magn {
		double SxAvg;
		double SyAvg;
		double SzAvg;
	};

	double rng(void) {
		return (double)(rand() / (double)RAND_MAX);
	}

	//Random theta [0,pi]
	double rng_theta() {
		double r = 1.0 / cos(1.0 - 2.0*rng());
		return r;
	}

	//Random phi [0,2*pi]
	double rng_phi() {
		double r = 2 * PI*rng();
		return r;
	}
public:
	Heisenberg() {
		for (int c = 0; c < COUNT; c++) {
			for (int i = 0; i < L; i++)
				for (int j = 0; j < L; j++)
					for (int k = 0; k < N; k++) {
						space[c][i][j][k][0] = rng_theta();
						space[c][i][j][k][1] = rng_phi();

					}
		}
		/*for(int i=0;i<L;i++)
		for(int j=0;j<L;j++)
		for(int k=0;k<N;k++) {
		space[0][i][j][k][0] = 0;
		space[0][i][j][k][1] = 0;
		}
		for(int i=0;i<L;i++)
		for(int j=0;j<L;j++)
		for(int k=0;k<N;k++) {
		space[1][i][j][k][0] = PI;
		space[1][i][j][k][1] = 0;
		}*/
	}

	void printSpace(int count) {
		cout << "count " << "[x,y,z] theta | phi" << "\n";
		for (int i = 0; i<L; i++)
			for (int j = 0; j<L; j++)
				for (int k = 0; k < N; k++)
					cout << count << " " << "[" << i << "," << j << "," << k << "] " << space[count][i][j][k][0] << " | " << space[count][i][j][k][1] << "\n";
	}
	void printSpaceForWolfram(int count) {
		//for Wolfram mathematica
		double sX, sY, sZ;
		cout << "\nvecs = {";
		for (int i = 0; i<L; i++)
			for (int j = 0; j<N; j++)
				for (int k = 0; k<L; k++) {
					sX = sin(space[count][i][j][k][0])*cos(space[count][i][j][k][1]);
					sY = sin(space[count][i][j][k][0])*sin(space[count][i][j][k][1]);
					sZ = cos(space[count][i][j][k][0]);
					printf("{{%d, %d, %d}, {%f, %f, %f}},", i, j, k, sX, sY, sZ);
					/*if(i != LENGTH-1 && j != LENGTH-1 && k != LENGTH-1)
					printf(",");*/
				}
		cout << "};";
	}

	double calculateEnergy(int count, int x, int y, int z) {
		double energy;
		double spinX = sin(space[count][x][y][z][0])*cos(space[count][x][y][z][1]);
		double spinY = sin(space[count][x][y][z][0])*sin(space[count][x][y][z][1]);
		double spinZ = cos(space[count][x][y][z][0]);
		int left = (x + L - 1) % L;
		int right = (x + 1) % L;
		int top = (z + N - 1) % N;
		int bot = (z + 1) % N;
		int back = (y + L - 1) % L;
		int front = (y + 1) % L;
		double sumAdjacentSpinX = 0.;
		double sumAdjacentSpinY = 0.;
		double sumAdjacentSpinZ = 0.;
		double sumAdjacentSpinX2 = 0.;
		double sumAdjacentSpinY2 = 0.;
		double sumAdjacentSpinZ2 = 0.;

		//для текущей пластины
		for (int i = 0; i<L; i++)
			for (int j = 0; j<L; j++)
				for (int k = 0; k<N; k++) {
					if (i == x && j == y && k == z)
						continue;
					sumAdjacentSpinX += sin(space[count][i][j][k][0])*cos(space[count][i][j][k][1]);
					sumAdjacentSpinY += sin(space[count][i][j][k][0])*sin(space[count][i][j][k][1]);
					sumAdjacentSpinZ += cos(space[count][i][j][k][0]);
				}
		//маленький быдло-код
		//проверяем слой из соседней пластины
		int nz; // номер слоя
		int nCount; //номер соседней пластины
		if (count == 0) {
			nz = 0;
			nCount = 1;
		}
		else if (count == 1) { //else if чтоб при модификации легче было искать ошибки
			nz = N - 1;
			nCount = 0;
		}
		for (int i = 0; i<L; i++)
			for (int j = 0; j<L; j++) {
				sumAdjacentSpinX2 += sin(space[nCount][i][j][nz][0]) * cos(space[nCount][i][j][nz][1]);
				sumAdjacentSpinY2 += sin(space[nCount][i][j][nz][0]) * sin(space[nCount][i][j][nz][1]);
				sumAdjacentSpinZ2 += cos(space[nCount][i][j][nz][0]);
			}
		energy = (-1.)* (spinX * sumAdjacentSpinX + spinY * sumAdjacentSpinY + spinZ * sumAdjacentSpinZ);
		energy += (0.1)* (spinX * sumAdjacentSpinX2 + spinY * sumAdjacentSpinY2 + spinZ * sumAdjacentSpinZ2);
		return energy;

	}

	double calculateEnergy2(int count, int x, int y, int z) {
		double energy; //-1 * (spinX * sumAdjacentSpinX + spinY * sumAdjacentSpinY + spinZ * sumAdjacentSpinZ)
		bool aaa = false;
		/*spinX = sin(theta)*cos(phi);
		* spinY = sin(theta)*sin(phi);
		* spinZ = cos(theta);*/

		double spinX = sin(space[count][x][y][z][0])*cos(space[count][x][y][z][1]);
		double spinY = sin(space[count][x][y][z][0])*sin(space[count][x][y][z][1]);
		double spinZ = cos(space[count][x][y][z][0]);
		int left = (x + L - 1) % L;
		int right = (x + 1) % L;
		int top = (z + N - 1) % N;
		int bot = (z + 1) % N;
		int back = (y + L - 1) % L;
		int front = (y + 1) % L;
		double sumAdjacentSpinX = 0.;
		double sumAdjacentSpinY = 0.;
		double sumAdjacentSpinZ = 0.;
		double sumAdjacentSpinX2 = 0.;
		double sumAdjacentSpinY2 = 0.;
		double sumAdjacentSpinZ2 = 0.;

		//  cout << "init_s:  " << count << " "  << "[" << x << "," << y << "," << z << "] " << space[count][x][y][z][0] << " | " << space[count][x][y][z][1] << "\n";
		//  cin.get();
		//left
		sumAdjacentSpinX += sin(space[count][left][y][z][0])*cos(space[count][left][y][z][1]);              //Xleft
		sumAdjacentSpinY += sin(space[count][left][y][z][0])*sin(space[count][left][y][z][1]);              //Yleft
		sumAdjacentSpinZ += cos(space[count][left][y][z][0]);                                               //Zleft

																											// cout << count << " left "  << "[" << left << "," << y << "," << z << "] " << space[count][left][y][z][0] << " | " << space[count][left][y][z][1] << "\n";
																											// cin.get();

																											//right
		sumAdjacentSpinX += sin(space[count][right][y][z][0])*cos(space[count][right][y][z][1]);            //Xright
		sumAdjacentSpinY += sin(space[count][right][y][z][0])*sin(space[count][right][y][z][1]);            //Yright
		sumAdjacentSpinZ += cos(space[count][right][y][z][0]);           //Zright

																		 // cout  << count << " right "  << "[" << right << "," << y << "," << z << "] " << space[count][right][y][z][0] << " | " << space[count][right][y][z][1] << "\n";
																		 // cin.get();

																		 //bottom
		if (!(count == 1 && z == N - 1)) {//�� ������� ������ ���� ������ ������ ��������
			if (count == 0 && z == N - 1) {//���� �� �������� ���� ������ ��������
				aaa = true;
				/*for(int f=0;f<L;f++) // ���-��-����� �� ������ ��������
				for(int g=0;g<L;g++)
				for(int h=0;h<N;h++){
				sumAdjacentSpinX2 += sin(space[1][f][g][h][0]) * cos(space[1][f][g][h][1]);                  //Xbottom
				sumAdjacentSpinY2 += sin(space[1][f][g][h][0]) * sin(space[1][f][g][h][1]);                  //Ybottom
				sumAdjacentSpinZ2 += cos(space[1][f][g][h][0]);                                              //Zbottom
				//   cout << 1 << " !1! "  << "[" << f << "," << g << "," << h << "] " << space[1][f][g][h][0] << " | " << space[1][f][g][h][1] << "\n";
				}*/
				//	cin.get();
				sumAdjacentSpinX2 += sin(space[1][x][y][0][0]) * cos(space[1][x][y][0][1]);                  //Xbottom
				sumAdjacentSpinY2 += sin(space[1][x][y][0][0]) * sin(space[1][x][y][0][1]);                  //Ybottom
				sumAdjacentSpinZ2 += cos(space[1][x][y][0][0]);                                            //Zbottom
			}
			else
			{
				sumAdjacentSpinX += sin(space[count][x][y][bot][0]) * cos(space[count][x][y][bot][1]);      //Xbottom
				sumAdjacentSpinY += sin(space[count][x][y][bot][0]) * sin(space[count][x][y][bot][1]);      //Ybottom
				sumAdjacentSpinZ += cos(space[count][x][y][bot][0]);                                        //Zbottom

																											//  cout << count << " "  << "[" << x << "," << y << "," << bot << "] " << space[count][x][y][bot][0] << " | " << space[count][x][y][bot][1] << "\n";
																											//  cin.get();
			}
		}
		//top
		if (!(count == 0 && z == 0)) { //�� ������� ������� ���� ������ ������� ��������
			if (count == 1 && z == 0) {//���� �� ������� ���� ������� ��������
				aaa = true;
				/*for(int f=0;f<L;f++)// ���-��-����� �� ������ ��������
				for(int g=0;g<L;g++)
				for(int h=0;h<N;h++){
				sumAdjacentSpinX2 += sin(space[0][f][g][h][0]) * cos(space[0][f][g][h][1]);              //Xtop
				sumAdjacentSpinY2 += sin(space[0][f][g][h][0]) * sin(space[0][f][g][h][1]);                //Ytop
				sumAdjacentSpinZ2 += cos(space[0][f][g][h][0]);                                            //Ztop

				//   cout  << 0 << " !0! "  << "[" << f << "," << g << "," << h << "] " << space[0][f][g][h][0] << " | " << space[0][f][g][h][1] << "\n";
				}
				*/	//	  cin.get();
				sumAdjacentSpinX2 += sin(space[0][x][y][N - 1][0]) * cos(space[0][x][y][N - 1][1]);                  //Xbottom
				sumAdjacentSpinY2 += sin(space[0][x][y][N - 1][0]) * sin(space[0][x][y][N - 1][1]);                  //Ybottom
				sumAdjacentSpinZ2 += cos(space[0][x][y][N - 1][0]);                                            //Zbottom

			}
			else
			{
				sumAdjacentSpinX += sin(space[count][x][y][top][0]) * cos(space[count][x][y][top][1]);      //Xtop
				sumAdjacentSpinY += sin(space[count][x][y][top][0]) * sin(space[count][x][y][top][1]);      //Ytop
				sumAdjacentSpinZ += cos(space[count][x][y][top][0]);                                        //Ztop

																											//  cout  << count << " top "  << "[" << top << "," << y << "," << z << "] " << space[count][x][y][top][0] << " | " << space[count][x][y][top][1] << "\n";
																											//  cin.get();
			}
		}
		//back
		sumAdjacentSpinX += sin(space[count][x][back][z][0])*cos(space[count][x][back][z][1]);              //Xback
		sumAdjacentSpinY += sin(space[count][x][back][z][0])*sin(space[count][x][back][z][1]);              //Yback
		sumAdjacentSpinZ += cos(space[count][x][back][z][0]);                                               //Zback

																											//  cout  << count << " back "  << "[" << x << "," << back << "," << z << "] " << space[count][x][back][z][0] << " | " << space[count][x][back][z][1] << "\n";
																											//  cin.get();

																											//front
		sumAdjacentSpinX += sin(space[count][x][front][z][0])*cos(space[count][x][front][z][1]);            //Xfront
		sumAdjacentSpinY += sin(space[count][x][front][z][0])*sin(space[count][x][front][z][1]);            //Yfront
		sumAdjacentSpinZ += cos(space[count][x][front][z][0]);                                              //Zfront

																											// cout  << count << " front "  << "[" << x << "," << front << "," << z << "] " << space[count][x][front][z][0] << " | " << space[count][x][front][z][1] << "\n";
																											//  cin.get();


		energy = (-1.)* (spinX * sumAdjacentSpinX + spinY * sumAdjacentSpinY + spinZ * sumAdjacentSpinZ);
		if (aaa)
			energy += (1.)* (spinX * sumAdjacentSpinX2 + spinY * sumAdjacentSpinY2 + spinZ * sumAdjacentSpinZ2);
		return energy;// J1=J2 = 1,  J12 = -0.1
	}

	double calculateFullEnergy() {
		double sumEnergy = 0;
		for (int c = 0;c<COUNT;c++)
			for (int i = 0; i<L; i++)
				for (int j = 0; j<N; j++)
					for (int k = 0; k<L; k++) {
						sumEnergy += calculateEnergy2(c, i, j, k);
					}
		return sumEnergy;
	}

	Magn calculateMagnetization(int count) {
		int NN = L * N * L;
		double sumSpinX = 0;
		double sumSpinY = 0;
		double sumSpinZ = 0;
		for (int i = 0; i<L; i++)
			for (int j = 0; j<N; j++)
				for (int k = 0; k<L; k++) {
					sumSpinX += sin(space[count][i][j][k][0])*cos(space[count][i][j][k][1]);
					sumSpinY += sin(space[count][i][j][k][0])*sin(space[count][i][j][k][1]);
					sumSpinZ += cos(space[count][i][j][k][0]);
				}
		double SxAvg = (1 / (double)NN)*sumSpinX;
		double SyAvg = (1 / (double)NN)*sumSpinY;
		double SzAvg = (1 / (double)NN)*sumSpinZ;
		Magn magn;
		magn.SxAvg = SxAvg;
		magn.SyAvg = SyAvg;
		magn.SzAvg = SzAvg;
		// cout << SxAvg << " " << SyAvg << " " << SzAvg << "\n";
		//double magnetization = SxAvg;//sqrt(SxAvg*SxAvg + SyAvg*SyAvg + SzAvg*SzAvg);// - SxAvg - SyAvg - SzAvg;// SxAvg + SyAvg + SzAvg; //
		return magn;
	}

	void metropolis() {
		double M = 0., E1 = 0., E2 = 0.;
		double ms1X[COUNT][3], ms2X[COUNT][3]; //0 -x, 1 - y, 2 - z
		double msAverage1[3], msAverage2[3];  //0 -x, 1 - y, 2 - z
		int mcs = 1000000, mcs_trial = 500;
		//T ->
		ofstream ofsMx("M_T_heisX.txt");
		ofstream ofsMy("M_T_heisY.txt");
		ofstream ofsMz("M_T_heisZ.txt");
		for (double t = 0.01; t < 3.1; t += 0.2) {
			//space(x,y,z)
			do
			{
				for (int i = 0;i<COUNT;i++) {
					ms1X[i][0] = 0.; ms2X[i][0] = 0.;
					ms1X[i][1] = 0.; ms2X[i][1] = 0.;
					ms1X[i][2] = 0.; ms2X[i][2] = 0.;
				}
				E1 = 0.; E2 = 0.;
				for (int i = 0;i<3;i++) {
					msAverage1[i] = 0.; msAverage2[i] = 0.;
				}

				for (int i = 0; i < mcs; i++) {
					int x = rand() % L;
					int y = rand() % N;
					int z = rand() % L;
					int c = rand() % COUNT;

					double energyOld = calculateEnergy2(c, x, y, z);

					double oldTheta = space[c][x][y][z][0];
					double oldPhi = space[c][x][y][z][1];
					double energyNew;
					double prob;

					space[c][x][y][z][0] = rng_theta();
					space[c][x][y][z][1] = rng_phi();

					energyNew = calculateEnergy2(c, x, y, z);

					if (energyNew >= energyOld)
						prob = exp((-1.)*(energyNew - energyOld) / t);
					else
						prob = 1;

					double p = ((double)rand()) / (double)RAND_MAX;
					if (prob < p) {
						space[c][x][y][z][0] = oldTheta;
						space[c][x][y][z][1] = oldPhi;
					}

					if (i>mcs - mcs_trial) {
						for (int j = 0;j<COUNT;j++) {

							ms1X[j][0] += calculateMagnetization(j).SxAvg;
							ms1X[j][1] += calculateMagnetization(j).SyAvg;
							ms1X[j][2] += calculateMagnetization(j).SzAvg;
						}
						// cout << "cm " << calculateMagnetization(0) << "\n";

						E1 += calculateFullEnergy() / ((double)L*N*L*COUNT);
						//}
					}
				}
				for (int i = 0;i<COUNT;i++) {

					ms1X[i][0] /= (double)mcs_trial;
					ms1X[i][1] /= (double)mcs_trial;
					ms1X[i][2] /= (double)mcs_trial;
				}
				//ofsM.close();
				for (int i = 0;i<COUNT;i++) {
					msAverage1[0] += ms1X[i][0];
					msAverage1[1] += ms1X[i][1];
					msAverage1[2] += ms1X[i][2];
				}

				msAverage1[0] /= (double)COUNT;
				msAverage1[1] /= (double)COUNT;
				msAverage1[2] /= (double)COUNT;
				E1 /= (double)mcs_trial;

				for (int i = 0; i < mcs; i++) {
					int x = rand() % L;
					int y = rand() % N;
					int z = rand() % L;
					int c = rand() % COUNT;

					double energyOld = calculateEnergy2(c, x, y, z);

					double oldTheta = space[c][x][y][z][0];
					double oldPhi = space[c][x][y][z][1];
					double energyNew;
					double prob;
					//10k iterations

					space[c][x][y][z][0] = rng_theta();
					space[c][x][y][z][1] = rng_phi();

					energyNew = calculateEnergy2(c, x, y, z);

					if (energyNew >= energyOld)
						prob = exp((-1.)*(energyNew - energyOld) / t);
					else
						prob = 1;

					double p = ((double)rand()) / (double)RAND_MAX;
					if (prob < p) {
						space[c][x][y][z][0] = oldTheta;
						space[c][x][y][z][1] = oldPhi;
					}

					if (i>mcs - mcs_trial) {
						for (int j = 0;j<COUNT;j++) {
							ms2X[j][0] += calculateMagnetization(j).SxAvg;
							ms2X[j][1] += calculateMagnetization(j).SyAvg;
							ms2X[j][2] += calculateMagnetization(j).SzAvg;
						}


						E2 += calculateFullEnergy() / ((double)L*N*L*COUNT);
					}
				}

				//ofsM.close();
				for (int i = 0;i<COUNT;i++) {
					ms2X[i][0] /= (double)mcs_trial;
					ms2X[i][1] /= (double)mcs_trial;
					ms2X[i][2] /= (double)mcs_trial;
				}

				//ofsM.close();
				for (int i = 0;i<COUNT;i++) {
					msAverage2[0] += ms2X[i][0];
					msAverage2[1] += ms2X[i][1];
					msAverage2[2] += ms2X[i][2];
				}

				msAverage2[0] /= (double)COUNT;
				msAverage2[1] /= (double)COUNT;
				msAverage2[2] /= (double)COUNT;

				E2 /= (double)mcs_trial;
				cout << t << "   " << E1 << " " << E2 << " " << E1 / E2 << " [x] = " << ((fabs(msAverage1[0]) + fabs(msAverage2[0])) / 2.) << " [y] = " <<
					((fabs(msAverage1[1]) + fabs(msAverage2[1])) / 2.) << " [z] = " << ((fabs(msAverage1[2]) + fabs(msAverage2[2])) / 2.) << " [x1] = " << ms2X[0][0] << " [x2] = " << ms2X[1][0] << "\n";

				if ( ((t < 0.02) && (((fabs(msAverage1[0]) + fabs(msAverage2[0])) / 2.) > 0.05)) )
				//	|| ((t < 0.02) && (((fabs(msAverage1[1]) + fabs(msAverage2[1])) / 2.) > 0.05))  )
						
						/*
						|| (((fabs(msAverage1[2]) + fabs(msAverage2[2])) / 2.) > 0.05))  )*/ // Чтобы гарантировано на 1м шаге по Т. была достигнута максимальное значение намагниченности
				{
					cout << ((fabs(msAverage1[0]) + fabs(msAverage2[0])) / 2.) << "  !!in if!!\n";
					E1 = 1.; E2 = 0.5;
				}
				cout << E1 << "  " << E2 << "\n";
 			} while ((fabs((E1 / E2)) <= 0.95) || (fabs((E1 / E2)) >= 1.05));

			ofsMx << t << " " << ms2X[0][0] << " " << ms2X[1][0] << " " << (msAverage1[0] + msAverage2[0]) / 2. << " " << (E1 + E2) / 2. << "\n";
			ofsMy << t << " " << ms2X[0][1] << " " << ms2X[1][1] << " " << (msAverage1[1] + msAverage2[1]) / 2. << " " << (E1 + E2) / 2. << "\n";
			ofsMz << t << " " << ms2X[0][2] << " " << ms2X[1][2] << " " << (msAverage1[1] + msAverage2[2]) / 2. << " " << (E1 + E2) / 2. << "\n";
			cout << t << " " << (msAverage1[0] + msAverage2[0]) / 2. << " !!! " << (E1 + E2) / 2. << "\n";

			//cin.get();
			// }
			//https://www.kontrolnaya-rabota.ru/s/grafik/tochka/
			//print E(T)
			// cout << t <<";" << calculateEnergy() << "\n";
			//printf M(T)
			//	M = calculateMagnetization();

			// cout << t <<" " << M << "\n";
			// cin.get();
		}
	}
};

int main() {
	srand((unsigned)time(NULL));
	Heisenberg heis;
	// heis.printSpace();
	cout << "--------------------------------------------\n";
	for (int i = 0;i<COUNT;i++)
		heis.printSpace(i);
	// cin.get();
	heis.metropolis();
	// heis.printSpace();
}
