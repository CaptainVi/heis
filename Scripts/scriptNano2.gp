## script.gp
#

#set term postscript eps enhanced font 'arial,14'

#set terminal postscript 'Helvetica, 12' eps enhanced solid  Helvetica
#set terminal postscript enhanced lw 2 24

#set term svg enhanced font 'arial,11' size 2048,1536 

#set format y "10^{%L}"

#set format y "%6.3e"

#set terminal postscript enhanced lw 2 20
set terminal png


#set terminal postscript enhanced eps lw 2 24
 #���� 
#set terminal postscript solid color enhanced lw 2 24
#set terminal postscript enhanced font 'arial,16' 
#set terminal bmp size 800,600
 
set encoding koi8r
#set terminal png

#set style data histograms




#dssdfs
#set style line 2 lt 5

#set output "Nano4.eps"
#set size 1.4,1.4

set xlabel "T" #, [K]"
#set xlabel "MK steps"
#set xlabel "{/Symbol m}H/J"
#set xlabel "Configuration number"
#set xlabel  "E,[erg]"
#set xlabel  "Distance [nm]"
#set xlabel "E{/Symbol \327}10^3^8, [erg]" 

#set ylabel  "dN/dT"
#set ylabel  "k_BT/J"
#set ylabel "J/|J|"
#set ylabel "E{/Symbol \327}10^3^8, [erg]"
#set ylabel "g(E)" #, [erg/K]"
set ylabel "<M>"

#set ylabel "E, [erg]"


#set xrange [-32:3.1]
#set yrange [1.8:2.2]
 
#set grid
set key top #center
set xzeroaxis  
set yzeroaxis
 
#
#lw 1 pt 1 w p 

#pt 1 ps 0.5
# yerrorbars  
#:(1.0) smooth acsplines
# 9 w p t
#5 w p t
#:3 with yerrorbars
#Ave. mod. of the magnetization 
#Frustration (N*10^1)

set output "M_T_layY.png"
plot "M_T_heisY.txt" u 1:2 pt 1 w p t 'm1Y' ,\
"M_T_heisY.txt" u 1:2 lw 2 smooth bezier with lines t '', "M_T_heisY.txt" u 1:3 pt 2 w p t 'm2Y',"M_T_heisY.txt" u 1:3 lw 2 smooth bezier with lines t '', "M_T_heisY.txt" u 1:4 pt 3 w p t 'mSumY' , "M_T_heisY.txt" u 1:4 lw 2 smooth bezier with lines t ''

set output "M_T_layX.png"
plot "M_T_heisX.txt" u 1:2 pt 1 w p t 'm1X' ,\
"M_T_heisX.txt" u 1:2 lw 2 smooth bezier with lines t '', "M_T_heisX.txt" u 1:3 pt 2 w p t 'm2X',"M_T_heisX.txt" u 1:3 lw 2 smooth bezier with lines t '', "M_T_heisX.txt" u 1:4 pt 3 w p t 'mSumX' , "M_T_heisX.txt" u 1:4 lw 2 smooth bezier with lines t ''

set output "M_T_layZ.png"
plot "M_T_heisZ.txt" u 1:2 pt 1 w p t 'm1Z' ,\
"M_T_heisZ.txt" u 1:2 lw 2 smooth bezier with lines t '', "M_T_heisZ.txt" u 1:3 pt 2 w p t 'm2Z',"M_T_heisZ.txt" u 1:3 lw 2 smooth bezier with lines t '', "M_T_heisZ.txt" u 1:4 pt 3 w p t 'mSumZ' , "M_T_heisZ.txt" u 1:4 lw 2 smooth bezier with lines t ''
